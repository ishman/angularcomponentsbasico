import { Component, Input, Output, OnInit, OnDestroy, OnChanges, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-countdown',
  templateUrl: './countdown.component.html',
  styleUrls: ['./countdown.component.scss']
})
export class CountdownComponent implements OnInit, OnDestroy, OnChanges {

  @Output() onDecrease = new EventEmitter<number>();
  @Output() onComplete = new EventEmitter<void>();

  @Input() init: number = null;
  public counter = 0;
  private counterTimerRef: any = null;

  constructor() { }

  ngOnInit(): void {
    this.startCountdown();
  }

  ngOnDestroy(): void {
    this.clearTimeOut();
  }

  ngOnChanges(): void {
    this.startCountdown();
  }

  startCountdown() {
    if ( this.init && this.init > 0 ) {
      this.clearTimeOut();
      this.counter = this.init;
      this.doCountdown();
    }
  }

  doCountdown() {
    this.counterTimerRef = setTimeout(() => {
      this.counter = this.counter - 1;
      this.processCountdown();
    }, 1000);
  }

  private clearTimeOut() {
    if (this.counterTimerRef) {
      clearTimeout(this.counterTimerRef);
      this.counterTimerRef = null;
    }
  }

  processCountdown() {
    this.onDecrease.emit(this.counter);
    console.log('Count is: ', this.counter);

    if ( this.counter === 0 ) {
      this.onComplete.emit();
      console.log('--Counter End--');
    } else {
      this.doCountdown();
    }
  }

}
